import java.io.*;


public class Main {
    public static void main(String[] args) throws IOException {
        String a = "adsfasdfasdf";

        // создание директории и файла
        File filePath = new File("C://Job");
        filePath.mkdir();
        File file1 = new File(filePath, "\\Example.txt");

        try {
            file1.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //запись переменной а в файл
        try(FileOutputStream fos=new FileOutputStream("C://Job//Example.txt"))
        {
            // перевод строки в байты
            byte[] buffer = a.getBytes();

            fos.write(buffer, 0, buffer.length);
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }

        // вывод содержимого файла на консоль
        BufferedReader in = new BufferedReader(new FileReader("C:\\Job\\Example.txt"));
        String line = in.readLine();
        while(line != null)
        {
            System.out.println(line);
            line = in.readLine();
        }
        in.close();

    }
}
