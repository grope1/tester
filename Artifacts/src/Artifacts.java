public class Artifacts {
    private int number;
    private String culture;
    private int century;


    public Artifacts (int number) {
        this.number = number;
            }

    public Artifacts (int number, String culture) {
        this.number = number;
        this.culture = culture;
    }

    public Artifacts (int number, String culture, int century) {
        this.number = number;
        this.culture = culture;
        this.century = century;
    }



}
