public class Phone {


//    public void call(String phoneNumber, String nameUser) {
//
//        System.out.println("Звонит: "+ nameUser + " Номер:" + phoneNumber);
//    }
//
//
//
//    public double getDiagonal(String s, int q, int r) {
//        double dia = Math.sqrt(q*q+r*r);
//        return dia;
//
//    }
//    public void phoneSwitchedOff() {
//        System.out.println("Телефон выключен");
//    }

    private int diagonal;

    public  Phone(String model){
        System.out.println("Модель телефона " + model);

    }

    public Phone(String model, String country){
        System.out.println("Модель телефона " + model + " Страна-производитель " + country);

    }

    public Phone(String model, String country, int year) {
        System.out.println("Модель телефона " + model + " Страна-производитель " + country + " Год выпуска " + year);
    }

    public Phone(String model, String country, int year, int diagonal) {
        this.diagonal = diagonal;
        System.out.println("Модель телефона " + model + " Страна-производитель " + country + " Год выпуска " + year +
                " Диагональ " + diagonal);
    }

}


